import React from 'react';
import { Link } from "react-router-dom";


class ShoesList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      shoes: [],
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/shoes/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ shoes: data.shoes })
    }

  }
  render() {
    return (
      <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-green">
        <h1 className="display-5 fw-bold">OMG. Shoes</h1>
        <div className="col-lg-6 mx-auto">
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoeadd" className="btn btn-primary btn-lg px-4 gap-3">Add Shoes</Link>
            <Link to="/ShoeRemove" className="btn btn-primary btn-lg px-4 gap-3">Remove Shoes</Link>
          </div>
        </div>
      </div>
      <nav

        style={{

          borderBottom: "solid 7px",
          paddingBottom: "1rem",
        }}
      >
      </nav>
      <h1> Available shoes</h1>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <table className="table table-image">
              <thead>
                <tr>
                  <th scope="col">Bin Number</th>
                  <th scope="col">Image</th>
                  <th scope="col">Manufacturer</th>
                  <th scope="col">Model</th>
                  <th scope="col">Color</th>
                </tr>
              </thead>
              <tbody>
                {this.state.shoes.map(shoe => {
                  return (
                    <tr key={shoe.id}   >
                      <th scope="row"> {shoe.location}</th>
                      <td className="w-25">
                        <img src={shoe.img_url} className="img-fluid img-thumbnail" alt="" />
                      </td>
                      <td>{shoe.manufacturer}</td>
                      <td>{shoe.model_name}</td>
                      <td>{shoe.color}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
    );
  };
}

export default ShoesList;
