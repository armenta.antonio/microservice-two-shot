import { BrowserRouter, Routes, Route, Outlet } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoesAddForm from './ShoesAddForm';

import HatsList from './HatsList';
import HatAddForm from './HatAddForm';
import HatRemove from './HatRemove';
import HatDeatils from './HatDetails';


function App(props) {
  console.log(props, "more props")
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesList shoes={props.shoes} />} />
          <Route path="/shoeadd" element={<ShoesAddForm />} />
          <Route path="/hats" element={<HatsList hats={props.hats}/>} />
          <Route path="/hatAddForm" element={<HatAddForm />} />
          <Route path="/hatRemove" element={<HatRemove />} />
          <Route path="/hatDetails" element={<HatDeatils />} />
        </Routes>



      </div>
    </BrowserRouter>
  );
}

export default App;
