import React from 'react';
import { Outlet, Link } from "react-router-dom";

class ShoesAddForm extends React.Component {
    constructor(props) {
      // console.log("test", props)
        super(props)
        this.state = {
            manufacturer: '',
            model_name: '',
            color: '',
            img_url: '',
        };
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleModelNameChange = this.handleModelNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleImgURLChange = this.handleImgURLChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState( {manufacturer: value})
    }

    handleModelNameChange(event) {
        const value = event.target.value;
        this.setState( {model_name: value})
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState( {color: value})
    }
    handleImgURLChange(event) {
        const value = event.target.value;
        this.setState( {img_url: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        this.props.history.push('/shoes');
        this.setState({ redirectToReferrer: true })
        const data = {...this.state};
        data.style_name = data.styleName;
        data.picture_url = data.pictureUrl
        delete data.styleName;
        delete data.pictureUrl;
        console.log(data);

        const locationUrl = 'http://localhost:8080/shoes/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          },
        };
  
        const response = await fetch(locationUrl, fetchConfig);
  
        if (response.ok) {  
          const newLocation = await response.json();
          console.log(newLocation);

          const cleared = {
            manufacturer: '',
            model_name: '',
            color: '',
            img_url: '',
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        // const url = 'http://wardrobe-api:8000/api/locations/';
        const url = "http://localhost:8080/shoes/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
        
            // // console.log("data", data)
            // const selectTag = document.getElementById("state");
            // for (let state of data.states) {
            //     const option = document.createElement("option");
            //     option.value = state.abbreviation;
            //     option.innerHTML = state.name;
            //     selectTag.appendChild(option);
            // }
        }
    }
  render() {
    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a pair of shoes </h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
                <input onChange={this.handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={this.state.manufacturer}/>
                <label htmlFor="manufacturer">manufacturer
                </label>
                
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleModelNameChange} placeholder="model_name" required type="text" name="model_name" id="model_name" className="form-control" value={this.state.model_name}/>
                <label htmlFor="fabric">model</label>
                
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" value={this.state.color}/>
                <label htmlFor="styleName">color</label>
                
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleImgURLChange} placeholder="img_url" required type="text" name="img_url" id="img_url" className="form-control" value={this.state.img_url}/>
                <label htmlFor="pictureUrl">add an image url</label>
                
              </div>

              <button className="btn btn-primary">create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    );
  }
}

export default ShoesAddForm;
