import React from 'react';
import {  Link } from "react-router-dom"

class HatRemoveForm extends React.Component {
    constructor(props) {
      console.log("test", props)
        super(props)
        this.state = {
            fabric: '',
            style_name: '',
            color: '',
            locations: [],
            id: [],
        };
        // this.handleFabricChange = this.handleFabricChange.bind(this);
        // this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
        // this.handleColorChange = this.handleColorChange.bind(this);
        // this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleIdChange = this.handleIdChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        

    }
    handleIdChange(event) {
        const value = event.target.value;
        this.setState( {id: value})
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        // data.room_count = data.roomCount;
        delete data.id;
        // delete data.states;
        console.log(data);

        const locationUrl = "http://localhost:8090/hat/${hat.id}";
        const fetchConfig = {
          method: "delete",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
  
        if (response.ok) {  
          const newDelete = await response.json();
          console.log(newDelete);

          const cleared = {
            fabric: '',
            style_name: '',
            color: '',
            locations: [],
            id: [],
            };
            this.setState(cleared);
        }
    }
    async componentDidMount() {
        const url = 'http://localhost:8090/hat/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({hats: data.hats});
        }
    }  
    render() {
        return (
            <>
        <nav>

        </nav>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Remove A Hat </h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">

              <div className="mb-3">
              <select onChange={this.handleIdChange} value={this.state.id} required name="id" id="id" className="form-select"  > 
                    <option value=''>Choose an Id to Delete</option>
                    {this.state.id.map(hats => {
                        return (
                        <option key={hats.id} value={hats.id}>
                            {hats.id}
                        </option>
                        );
                    })}
                    </select>
                   
              </div>
              <button className="btn btn-primary">Delete</button>
              {/* <button  className="btn btn-danger"  onClick={() => this.deleteC(this.state.id)}>Remove</button> */}
        
            </form>
          </div>
        </div>
      </div>
       </>
        );
    }
}

    export default HatRemoveForm;