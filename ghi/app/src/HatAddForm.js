import React from 'react';
import { Outlet, Link } from "react-router-dom"

class HatAddForm extends React.Component {
    constructor(props) {
      // console.log("test", props)
        super(props)
        this.state = {
            fabric: '',
            styleName: '',
            pictureUrl: '',
            color: '',
            location: "",
        };
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState( {fabric: value})
    }

    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState( {styleName: value})
    }
    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState( {pictureUrl: value})
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState( {color: value})
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState( {location: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.style_name = data.styleName;
        data.picture_url = data.pictureUrl
        delete data.styleName;
        delete data.pictureUrl;
        console.log(data);

        const locationUrl = 'http://localhost:8090/hat/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          },
        };
  
        const response = await fetch(locationUrl, fetchConfig);
  
        if (response.ok) {  
          const newLocation = await response.json();
          console.log(newLocation);

          const cleared = {
              fabric: '',
              styleName: '',
              picture_url: '',
              color: '',
              location: '',
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        // const url = 'http://wardrobe-api:8000/api/locations/';
        const url = "http://localhost:8090/hat/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
        
            // // console.log("data", data)
            // const selectTag = document.getElementById("state");
            // for (let state of data.states) {
            //     const option = document.createElement("option");
            //     option.value = state.abbreviation;
            //     option.innerHTML = state.name;
            //     selectTag.appendChild(option);
            // }
        }
    }
  render() {
    return (
        <>
        <nav>
        {/* <Link to="/hatRemove">Remove Hats</Link> |{"  "}
        <Link to="/hatDetails">Hat Details</Link> */}
        </nav>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Hat </h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
                <input onChange={this.handleLocationChange} placeholder="Location" required type="text" name="location" id="location" className="form-control" value={this.state.location}/>
                <label htmlFor="location">Is this needed?
                </label>
                
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={this.state.fabric}/>
                <label htmlFor="fabric">Fabric</label>
                
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStyleNameChange} placeholder="sstyleName" required type="text" name="styleName" id="styleName" className="form-control" value={this.state.styleName}/>
                <label htmlFor="styleName">Style Name</label>
                
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePictureUrlChange} placeholder="pictureUrl" required type="text" name="pictureUrl" id="pictureUrl" className="form-control" value={this.state.pictureUrl}/>
                <label htmlFor="pictureUrl">Add a http:// link to your hat Or put "Null"</label>
                
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={this.state.color}/>
                <label htmlFor="color">Color</label>
                
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    );
  }
}

export default HatAddForm;


// function HatAddForm(props) {

//     return(
//         <div>
//             <nav>
//             <Link to="/hatRemove">Remove Hats</Link> |{"  "}
//             <Link to="/hatDetails">Hat Details</Link>
//             </nav>
//             <h2> hats</h2>
//             <Outlet />

//         </div>
//     )
// }


// export default HatAddForm;
