import React from 'react';
import Nav from './Nav';
import { Link } from "react-router-dom"


function HatsList(props) {
console.log(props, "props")
   if (props.hats === undefined)
      { console.log("we got nohat", props.hats)
        return null

      }
    return(
        <> 
        <div className="px-4 py-5 my-5 mt-0 text-center bg-green">
          <h1 className="display-5 fw-bold">I LOVE MY HATS</h1>
          <div className="col-lg-6 mx-auto">
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/hatAddForm" className="btn btn-primary btn-lg px-4 gap-3">Add Hats</Link>
              <Link to="/hatRemove" className="btn btn-primary btn-lg px-4 gap-3">Remove Hats</Link>
              
            </div>
          </div>
        </div>
        <nav
 
        style={{
            
          borderBottom: "solid 7px",
          paddingBottom: "1rem",
        }}
      >
        {/* <Link to="/hatDetails">Hat Details</Link> */}
      </nav>
      <h1> Look at all my Hats</h1>
      <div className="container">
  <div className="row">
    <div className="col-12">
		<table className="table table-image">
		  <thead>
		    <tr>
		      <th scope="col">Number/location</th>
		      <th scope="col">Image</th>
		      <th scope="col">Fabric</th>
		      <th scope="col">Style</th>
		      <th scope="col">Color</th>
		      <th scope="col">ID#</th>
		    </tr>
		  </thead>
		  <tbody>
            {props.hats.map(hat => {
                return (
            <tr key={hat.id}   >
		      <th scope="row"> { hat.location }</th>
		      <td className="w-25">
			      <img src={hat.picture_url}  className="img-fluid img-thumbnail" alt=""/>
		      </td>
		      <td>{ hat.fabric }</td>
		      <td>{ hat.style_name }</td>
		      <td>{ hat.color }</td>
		      <td>{ hat.id }</td>
		    </tr>
              );
            })}
		  </tbody>
		</table>   
    </div>
  </div>
</div>
        {/* <table className="table table-striped">
        
          <thead> 
            <tr>
              <th>Fabric</th>
              <th>Style Name</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Location</th>
              <th> ID #</th>
            </tr>
          </thead>
          <tbody>
              
            {props.hats.map(hat => {
              return (
                <tr key={hat.id}   >
                 
                  <td>{ hat.fabric }</td> 
                  <td>{ hat.style_name } </td>
                  <td>{ hat.color }</td>
                  <td>{ hat.picture_url }</td>
                  <td>{ hat.location }</td>
                  <td>{ hat.id }</td>
                </tr>
              );
            })}
          </tbody>
        </table> */}
        </>
    )
}

export default HatsList;
