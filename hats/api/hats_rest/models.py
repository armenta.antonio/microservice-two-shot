from django.db import models
from django.urls import reverse

 

# Create your models here.


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True, blank=True)
    location = models.CharField(max_length=200)

    # location = models.ForeignKey(
    #     LocationsVO, 
    #     related_name="attendees",
    #     on_delete=models.CASCADE
    # )

    # ("closet_name", "section_number", "shelf_number")

    def get_api_url(self):
        return reverse("api_list_hat", kwargs={'pk': self.pk})

    def _str_(self):
        return self.name

class LocationsVO (models.Model):
    name = models.CharField(max_length=200)
    import_href= models.CharField(max_length=200, unique=True)
    
