from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationsVO



class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
       
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {
        "name": LocationsVO(),
     }
    # def get_extra_data(self, o):
    #     count = LocationsVO.objects.filter(name=o.name).count()
    #     return {"location": count > 0}

@require_http_methods(["GET", "POST"])
def api_list_hat(request):
    if request.method == "GET":
        hat = Hat.objects.all()
        return JsonResponse(
            {"hats": hat},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat, 
            encoder=HatListEncoder,
            safe=False,
        )
@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hat(request, pk):

    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat, 
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})
    return JsonResponse(
        hat, 
        encoder= HatDetailEncoder,
        safe=False,
   )

