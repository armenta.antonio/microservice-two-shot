import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from hats_rest.models import Something

# from hats_rest.models import Something

from hats_rest.models import LocationsVO

def get_wardrobe_locations():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    print("response", response,)
    content = json.loads(response.content)
    for location in content["locations"]:
        LocationsVO.objects.update_or_create(
            import_href=location["closet-name"],
            defaults={"closest_name": location["closet_name"]},
            # "closet_name", "section_number", "shelf_number"
        )

def poll():
    while True:
        print('Hats poller polling for data')
        try:
            get_wardrobe_locations()
            # Write your polling logic, here
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()


